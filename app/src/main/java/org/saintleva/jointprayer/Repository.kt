/*
 * Copyright (C) Anton Liaukevich 2021-2022 <leva.dev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.saintleva.jointprayer

import androidx.compose.runtime.mutableStateOf
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import org.saintleva.jointprayer.firebase.EventResponse
import org.saintleva.jointprayer.firebase.singleValueEvent
import org.saintleva.jointprayer.models.GroupToSelect
import org.saintleva.jointprayer.models.Role
import java.lang.Exception
import java.security.acl.Group


object Repository {

    var myGroups = mutableStateOf(mapOf<String, GroupToSelect>())
    var myCurrentGroupId = mutableStateOf<String?>(null)

    val myCurrentGroup: GroupToSelect?
        get() {
            return if (myCurrentGroupId.value != null)
                myGroups.value[myCurrentGroupId.value]
            else
                null
        }

    val userId = "zeDtANS9SifRfw4M94HDCXpXEt83"

    suspend fun loadMyGroups() {
        try {
            val newMyGroups = mutableMapOf<String, GroupToSelect>()
            val db = Firebase.database.reference
            val groupsRef = db.child("users").child(userId).child("groups")
            val groupsResult = groupsRef.singleValueEvent()
            if (groupsResult is EventResponse.Cancelled)
                throw RealtimeDatabaseException(groupsResult.error)
            val groupsSnapshot = (groupsResult as EventResponse.Changed).snapshot
            for (groupEntry in (groupsSnapshot.value as Map<String, Boolean>).asIterable()) {
                val myGroupRef = db.child("groups").child(groupEntry.key)
                val nameResult = myGroupRef.child("name").singleValueEvent()
                if (nameResult is EventResponse.Cancelled)
                    throw RealtimeDatabaseException(nameResult.error)
                val nameSnapshot = (nameResult as EventResponse.Changed).snapshot

                val roleResult = myGroupRef.child("members").child(userId).singleValueEvent()
                if (roleResult is EventResponse.Cancelled)
                    throw RealtimeDatabaseException(roleResult.error)
                val roleSnapshot = (roleResult as EventResponse.Changed).snapshot

                val closedResult = myGroupRef.child("closed").singleValueEvent()
                if (closedResult is EventResponse.Cancelled)
                    throw RealtimeDatabaseException(closedResult.error)
                val closedSnapshot = (closedResult as EventResponse.Changed).snapshot

                val groupToSelect = GroupToSelect(
                    name = nameSnapshot.value as String,
                    notifications = groupEntry.value,
                    myRole = when (roleSnapshot.value as String) {
                        "administrator" -> Role.ADMINISTRATOR
                        "moderator" -> Role.MODERATOR
                        "user" -> Role.USER
                        "banned" -> Role.BANNED
                        else -> throw DatabaseFormatException("invalid role")
                    },
                    closed = closedSnapshot.value as Boolean
                )
                newMyGroups[groupEntry.key] = groupToSelect
            }
            myGroups.value = newMyGroups
            val currentGroupRef = db.child("users").child(userId).child("currentGroup")
            val currentGroupResult = currentGroupRef.singleValueEvent()
            if (currentGroupResult is EventResponse.Cancelled)
                throw RealtimeDatabaseException(currentGroupResult.error)
            val currentGroupSnapshot = (currentGroupResult as EventResponse.Changed).snapshot
            myCurrentGroupId.value = currentGroupSnapshot.value as String?
        } catch (e: DatabaseException) {
            throw e
        } catch (e: Exception) {
            throw DatabaseFormatException(e.message)
        }
    }
}