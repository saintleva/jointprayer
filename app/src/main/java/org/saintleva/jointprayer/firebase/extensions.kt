package org.saintleva.jointprayer.firebase

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import java.lang.Exception


sealed interface AddingElementToListError {
    val message: String

    class IsNotList: AddingElementToListError {
        override val message = "Node you are trying to add element is not list"
    }

    class ErrorInDatabase(val error: DatabaseError): AddingElementToListError {
        override val message: String
            get() = error.message
    }
}

interface AddingElementToListListener {
    fun onSuccessed()
    fun onCancelled(error: AddingElementToListError)
    fun onFailure(exception: Exception)
}

fun DatabaseReference.addElementToList(value: Any?, listener: AddingElementToListListener) {
    val ref = this
    this.addListenerForSingleValueEvent(
        object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.value == null)
                    ref.child("0").setValue(value)
                        .addOnSuccessListener {
                            listener.onSuccessed()
                        }
                        .addOnFailureListener() {
                            listener.onFailure(it)
                        }
                else if (snapshot.value is List<*>) {
                    val size = snapshot.childrenCount
                    ref.child("$size").setValue(value)
                        .addOnSuccessListener {
                            listener.onSuccessed()
                        }
                        .addOnFailureListener() {
                            listener.onFailure(it)
                        }
                }
                else
                    listener.onCancelled(AddingElementToListError.IsNotList())
            }

            override fun onCancelled(error: DatabaseError) {
                listener.onCancelled(AddingElementToListError.ErrorInDatabase(error))
            }
        })
}