/*
 * Copyright (C) Anton Liaukevich 2021-2022 <leva.dev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.saintleva.jointprayer.firebase

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


sealed class EventResponse {
    data class Changed(val snapshot: DataSnapshot): EventResponse()
    data class Cancelled(val error: DatabaseError): EventResponse()
}

suspend fun DatabaseReference.singleValueEvent(): EventResponse {
    return suspendCoroutine { continuation ->
        val valueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                continuation.resume(EventResponse.Cancelled(error))
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                continuation.resume(EventResponse.Changed(snapshot))
            }
        }
        addListenerForSingleValueEvent(valueEventListener) // Subscribe to the event
    }
}

//TODO: remove it
//suspend fun DatabaseReference.singleGet(): EventResponse {
//    return suspendCoroutine { continuation ->
//        get().addOnSuccessListener {
//            continuation.resume(EventResponse.Changed(it))
//        }.addOnFailureListener {
//            continuation.resume(EventResponse.Cancelled(DatabaseError.fromException(it)))
//        }.addOnCanceledListener {
//            continuation.resume(EventResponse.Cancelled(DatabaseError.fromStatus("cancelled")))
//        }
//    }
//}