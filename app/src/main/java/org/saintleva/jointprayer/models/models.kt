package org.saintleva.jointprayer.models

import com.google.firebase.database.IgnoreExtraProperties


@IgnoreExtraProperties
class Need(
    val author: String,
    val text: String
//    val type: String,
//    val confidential: Boolean,
//    val created: Timestamp,
//    val modified: Timestamp
)

enum class Role {
    ADMINISTRATOR, MODERATOR, USER, BANNED
}

data class GroupToSelect(
    var name: String,
    var notifications: Boolean,
    var myRole: Role,
    var closed: Boolean
)