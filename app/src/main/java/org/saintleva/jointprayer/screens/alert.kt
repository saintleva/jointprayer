/*
 * Copyright (C) Anton Liaukevich 2021-2022 <leva.dev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.saintleva.jointprayer.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import org.saintleva.jointprayer.DatabaseFormatException
import org.saintleva.jointprayer.R
import org.saintleva.jointprayer.RealtimeDatabaseException
import org.saintleva.jointprayer.viewmodels.DatabaseViewModel


@Composable
fun DatabaseErrorAlert(vm: DatabaseViewModel, onClose: () -> Unit) {
    if (vm.error.value != null) {
        AlertDialog(
            onDismissRequest = { onClose() },
            title = { Text(text = stringResource(R.string.error)) },
            text = { Text(
                text = when(vm.error!!.value) {
                    is RealtimeDatabaseException ->
                        stringResource(R.string.firebase_database_internal_error) + ": ${vm.error.toString()}"
                    is DatabaseFormatException ->
                        stringResource(R.string.firebase_database_format_error)
                    else -> stringResource(R.string.another_error)
                })
            },
            buttons = {
                Row(
                    modifier = Modifier.padding(all = 8.dp),
                    horizontalArrangement = Arrangement.Center
                ) {
                    Button(
                        onClick = { onClose() }
                    ) {
                        Text("OK", fontSize = 22.sp)
                    }
                }
            }
        )
    }
}