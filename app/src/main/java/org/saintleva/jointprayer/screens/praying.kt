/*
 * Copyright (C) Anton Liaukevich 2021-2022 <leva.dev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.saintleva.jointprayer.screens

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Face
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.List
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import kotlinx.coroutines.launch
import org.saintleva.jointprayer.R
import org.saintleva.jointprayer.Repository
import org.saintleva.jointprayer.models.Role


@Composable
fun PrayingScreen(navController: NavController) {
    val scaffoldState = rememberScaffoldState()
    val scope = rememberCoroutineScope()

    Scaffold(
        scaffoldState = scaffoldState,
        //drawerContent =  { SelectGroupScreen(generateGroups(100)) },
        topBar = {
            TopAppBar {
                IconButton(
                    onClick = {
                        scope.launch {
                            scaffoldState.drawerState.open()
                        }
                    }
                ) {
                    Icon(Icons.Filled.Face, contentDescription = "Navigation Drawer")
                }
                IconButton(
                    onClick = { navController.navigate("selectgroup") }
                ) {
                    Icon(Icons.Filled.List, contentDescription = "Group selection")
                }
            }
        }
    ) {
        Column(modifier = Modifier
            .fillMaxSize()
            .background(Color.Green)
        ) {
            Text(Repository.myCurrentGroupId.value ?: "null")
//            val group = Repository.myCurrentGroup
//            Log.i("compose", "group: $group")
//            if (group == null)
//                Text("User is not a member of any group")
//            else {
//                Text(group.name)
//                Text(text = when(group.myRole) {
//                    Role.ADMINISTRATOR -> stringResource(R.string.role_administrator)
//                    Role.MODERATOR -> stringResource(R.string.role_moderator)
//                    Role.USER -> stringResource(R.string.role_user)
//                    Role.BANNED -> stringResource(R.string.role_banned)
//                })
//                Text(
//                    text = when (group.closed) {
//                        false -> stringResource(R.string.group_opened)
//                        true -> stringResource(R.string.group_closed)
//                    },
//                )
//                Text("Send push-notifications for this group: ${group.notifications}")
//            }
        }
    }
}