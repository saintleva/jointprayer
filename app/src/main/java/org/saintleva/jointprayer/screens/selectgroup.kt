/*
 * Copyright (C) Anton Liaukevich 2021-2022 <leva.dev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.saintleva.jointprayer.screens

import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role.Companion.Button
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.saintleva.jointprayer.DatabaseFormatException
import org.saintleva.jointprayer.R
import org.saintleva.jointprayer.RealtimeDatabaseException
import org.saintleva.jointprayer.Repository
import org.saintleva.jointprayer.models.GroupToSelect
import org.saintleva.jointprayer.models.Role
import org.saintleva.jointprayer.ui.theme.groupName
import org.saintleva.jointprayer.ui.theme.groupType
import org.saintleva.jointprayer.viewmodels.SelectGroupViewModel


@Composable
fun GroupCard(id: String, group: GroupToSelect, onGroupChange: (String) -> Unit) {
    Card(
        elevation = 8.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    color = if (id == Repository.myCurrentGroupId.value)
                        MaterialTheme.colors.secondary
                    else
                        MaterialTheme.colors.surface
                )
                .clickable { onGroupChange(id) }
        ) {
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = group.name,
                    modifier = Modifier

                        .fillMaxWidth(0.8f)
                        .padding(all = 8.dp),
                    style = MaterialTheme.typography.groupName
                )
                Text(
                    text = when (group.closed) {
                        false -> stringResource(R.string.group_opened)
                        true -> stringResource(R.string.group_closed)
                    },
                    modifier = Modifier
                        .padding(all = 8.dp)
                        .align(Alignment.Top),
                    style = MaterialTheme.typography.groupType,
                    softWrap = false
                )
            }
            Text(
                text = stringResource(R.string.my_role_is) + ": " + when (group.myRole) {
                    Role.ADMINISTRATOR -> stringResource(R.string.role_administrator)
                    Role.MODERATOR -> stringResource(R.string.role_moderator)
                    Role.USER -> stringResource(R.string.role_user)
                    Role.BANNED -> stringResource(R.string.role_banned)
                },
                modifier = Modifier.padding(horizontal = 8.dp, vertical = 4.dp),
                style = MaterialTheme.typography.caption
            )
        }
    }
}

@Composable
fun SelectGroupScreen(navController: NavHostController, vm: SelectGroupViewModel) {
    DatabaseErrorAlert(vm, onClose = { navController.navigateUp() })
    CircularProgressIndicator()
    LazyColumn() {
        val groups = Repository.myGroups.value
        for (key in groups.keys)
            item {
                GroupCard(
                    key,
                    //TODO: Do I really need to use "!!"?
                    groups[key]!!, {
                        Repository.myCurrentGroupId.value = it
                        navController.navigate("praying")
                    }
                )
            }
    }
}