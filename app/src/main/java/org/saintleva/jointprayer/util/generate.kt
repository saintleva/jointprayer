/*
 * Copyright (C) Anton Liaukevich 2021-2022 <leva.dev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.saintleva.jointprayer.util

import org.saintleva.jointprayer.models.GroupToSelect
import org.saintleva.jointprayer.models.Role


//fun generateGroups(count: Int): List<GroupToSelect> {
//
//    fun generateString(length: Int): String {
//        val result = StringBuilder()
//        for (i in 0 until length)
//            result.append("a")
//        return result.toString()
//    }
//
//    val result = mutableListOf<GroupToSelect>()
//    for (i in 0 until count)
//        result.add(
//            GroupToSelect(
//                id = i,
//                name = generateString(i),
//                myRole = when (i % 4) {
//                    0 -> Role.ADMINISTRATOR
//                    1 -> Role.MODERATOR
//                    2 -> Role.USER
//                    3 -> Role.BANNED
//                    else -> Role.BANNED
//                },
//                closed = when (i % 2) {
//                    0 -> false
//                    1 -> true
//                    else -> false
//                }
//            )
//        )
//    result[2].name = "Церковь евангельских христиан баптистов в г. Слуцке"
//    result[3].name = "Minsk New Land Church"
//    return result
//}