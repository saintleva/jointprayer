/*
 * Copyright (C) Anton Liaukevich 2021-2022 <leva.dev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.saintleva.jointprayer.viewmodels

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.saintleva.jointprayer.DatabaseException
import org.saintleva.jointprayer.Repository
import org.saintleva.jointprayer.models.GroupToSelect
import java.lang.Exception


class SelectGroupViewModel : DatabaseViewModel() {

//    private var _groups = mutableStateOf(listOf<GroupToSelect>())
//    val groups: State<List<GroupToSelect>>
//        get() = _groups

    fun loadGroups() {
        doOperation {
            Repository.loadMyGroups()
//            _groups.value = Repository.loadMyGroups()
        }
    }
}